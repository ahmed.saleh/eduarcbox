# EduARC Docker Container

Provides an easy to use development environment for the EduARC platform.

## Prerequisites

The following software packages have to be present:

- [Git](https://git-scm.com/)
- On Windows 7
  - [VirtualBox](https://www.virtualbox.org/)
  - [Docker toolbox](https://docs.docker.com/toolbox/toolbox_install_windows/)
    - [Installer for Windows 7](https://github.com/docker/toolbox/releases)
- On Linux
  - [Docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
  - [Docker Compose](https://docs.docker.com/compose/install/)

If missing follow the instruction on the linked websites to obtain the software.
At the time of writing latest Docker version is 18.09.3. Tested on Windows with Docker 18.09.3 and VirtualBox 5.2.20 (version used in the Docker toolbox installer) and on Ubuntu 16.04 with Docker 18.09.3 and Docker Compose 1.24.0.

**WARNING** On Windows (7) Docker toolbox is not working smoothly, suggested solution is to use a Linux VM as exaplained [here](https://medium.com/faun/hey-docker-why-you-hate-windows-so-much-de7a7aa4dd7).


## EduARC box intro

For a good docker introduction see:

- [Official Docker _Get started_ tutorial](https://docs.docker.com/get-started/)
- [Another useful tutorial](https://docker-curriculum.com/)

At now there are two containers:

- `eduarc`. The container consist of an Ubuntu 18.04 environment with PHP 7.3.6-cli, MySQL 5.7, Apache 2.4.29, and PhPMyAdmin 4.9.0.1. `eduarc` is based on [DockerLAMP](https://github.com/mattrayner/docker-lamp), which adds a LAMP stack to [Baseimage-docker](https://github.com/phusion/baseimage-docker) (a lightweight Ubuntu image).
- `jenkins` 

The file `docker-compose.yml` is the main file for building the development environment.

## Usage & Docker commands

After cloning `eduarcbox`, please clone the repository `eduarc-webapp` into the following directory `eduarcbox/eduarc-app`.  
Note: in the future, the last step will be automated with the GIT submodule.

To run the EduARC box from `eduarcbox/` you may use the following commands to:

- start the docker container (if not built before also build it): `docker-compose up -d`
- start the docker container (if built before): `docker-compose start -d`
- shutdown the docker container (and removes containers, networks, volumes, and images created by up): `docker-compose down`
- build the docker container: `docker-compose build`
- rebuild and start the docker container: `docker-compose up -d --build`
- stop docker containers (without deleting it): `docker-compose stop`
- view output from containers `docker-compose logs`

After installation the EduARC platform can accessed at [http://192.168.99.100:3000](http://192.168.99.100:3000).  
Jenknis is accessible at [http://192.168.99.100:3080](http://192.168.99.100:3080)  
PhPMyAdmin is accessible at [http://192.168.99.100:3000/phpmyadmin](http://192.168.99.100:3000/phpmyadmin).

### MySQL in eduarc container

By default, the eduarc image comes with a root MySQL account that has a random password. This account is only available locally, i.e. within your application. It is not available from outside your docker image or through phpMyAdmin. When you first run the image you'll see a message showing your admin user's password. This user can be used locally and externally, either by connecting to your MySQL port (default 3306) and using a tool like MySQL Workbench or Sequel Pro, or through phpMyAdmin. If you need this login later, you can run `docker logs CONTAINER_ID` and you should see it at the top of the log. Optionally, you can change the password for the admin a nd root user:

1. Open a shell to run arbitrary commands inside an existing container and open mysql

```
docker exec -it <container_name> bash
mysql -u root
```

2. Update the password for the admin user

```sql
set password for 'admin'@'localhost' = PASSWORD('mypass');
```

**WARNING** Changing password has to be repeated every time the eduarcbox is built.



### Other useful commands

```bash
## List Docker CLI commands
docker
docker container --help

## Display Docker version and info
docker --version
docker version
docker info

## Execute Docker image
docker run hello-world

## List Docker images
docker image ls

## List Docker containers (running, all, all in quiet mode)
docker container ls
docker container ls --all
docker container ls -aq

## Check used space
docker system df

## Clean docker (what is no more used, also volumes, whatever is not running)
docker system prune
docker system prune --volumes
docker system prune -a
```

## Useful links

- [Official Docker _Get started_ tutorial](https://docs.docker.com/get-started/)
- [Another useful tutorial](https://docker-curriculum.com/)
- [Docker on Windows through a Ubuntu VM](https://medium.com/faun/hey-docker-why-you-hate-windows-so-much-de7a7aa4dd7)
- [DockerLAMP](https://github.com/mattrayner/docker-lamp)
- [Baseimage-docker](https://github.com/phusion/baseimage-docker)
- [Moodle docker image](https://github.com/docker-scripts/moodle)
- [Create a continous integration pipeline with Jenknis and GitLab](https://docs.bitnami.com/aws/how-to/create-ci-pipeline/)
- [Running Ansible on Docker](http://mauricioklein.com/ansible/docker/2018/02/23/ansible-docker/)


### Clone eduarc-moodle-webapp

- Store git credentials in: `eduarc-build/gitlab-credentials.txt`
