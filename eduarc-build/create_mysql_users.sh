#!/bin/bash

/usr/bin/mysqld_safe > /dev/null 2>&1 &

RET=1
while [[ RET -ne 0 ]]; do
    echo "=> Waiting for confirmation of MySQL service startup"
    sleep 5
    mysql -uroot -e "status" --password="" > /dev/null 2>&1
    RET=$?
done

#Import DB
DB_MOODLE_DB_NAME=moodle
DB_MOODLE_USER=moodleuser
DB_MOODLE_PASSWORD=mypassword
DB_MOODLE_DB_FILENAME=setup_database_eduarc-moodle-webapp.sql

PASS=${MYSQL_ADMIN_PASS:-$(pwgen -s 12 1)}
_word=$( [ ${MYSQL_ADMIN_PASS} ] && echo "preset" || echo "random" )
echo "=> Creating MySQL admin user with ${_word} password"

mysql -uroot --password=""  -e "CREATE USER 'admin'@'%' IDENTIFIED BY '$PASS'"
mysql -uroot --password=""  -e "GRANT ALL PRIVILEGES ON *.* TO 'admin'@'%' WITH GRANT OPTION"

# Create initial moodle user and the moodle database 
mysql -uroot --password=""  -e "CREATE DATABASE moodle DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci"
mysql -uroot --password=""  -e "GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,CREATE TEMPORARY TABLES,DROP,INDEX,ALTER,LOCK TABLES ON moodle.* TO 'moodleuser'@'localhost' IDENTIFIED BY 'mypassword'"
echo "========================================================================"

echo "Starting to import MOODLE DB..."
cat $DB_MOODLE_DB_FILENAME | mysql moodle -uroot  --password=""

echo "Import complete..."
echo "enjoy!"
echo "========================================================================"

#mysql -uroot -e " GRANT ALL PRIVILEGES ON phpmyadmin.* TO  'pma'@'localhost' IDENTIFIED BY ''"

CREATE_MYSQL_USER=false

if [ -n "$CREATE_MYSQL_BASIC_USER_AND_DB" ] || \
   [ -n "$MYSQL_USER_NAME" ] || \
   [ -n "$MYSQL_USER_DB" ] || \
   [ -n "$MYSQL_USER_PASS" ]; then
      CREATE_MYSQL_USER=true
fi

if [ "$CREATE_MYSQL_USER" = true ]; then
    _user=${MYSQL_USER_NAME:-user}
    _userdb=${MYSQL_USER_DB:-db}
    _userpass=${MYSQL_USER_PASS:-password}

    mysql -uroot --password="" -e "CREATE USER '${_user}'@'%' IDENTIFIED BY  '${_userpass}'"
    mysql -uroot --password="" -e "GRANT USAGE ON *.* TO  '${_user}'@'%' IDENTIFIED BY '${_userpass}'"
    mysql -uroot --password="" -e "CREATE DATABASE IF NOT EXISTS ${_userdb}"
    mysql -uroot --password="" -e "GRANT ALL PRIVILEGES ON ${_userdb}.* TO '${_user}'@'%'"
fi

####################################################################################################################
exec /import_moodle_db.sh &
#########################################################################################################################

# mysql secure
DB_ROOT_PASSWORD=$(pwgen -s 12 1)
mysql --user=root --password="" <<_EOF_
set password for 'root'@'localhost' = PASSWORD('${DB_ROOT_PASSWORD}');
DELETE FROM mysql.user WHERE User='';
DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');
DROP DATABASE IF EXISTS test;
DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';
FLUSH PRIVILEGES;
_EOF_

echo "=> Done!"

echo "========================================================================"
echo "You can now connect to this MySQL Server with $PASS"
echo ""
echo "    mysql -uadmin -p$PASS -h<host> -P<port>"
echo ""
echo "Please remember to change the above password as soon as possible!"
echo "MySQL user 'root' has password $DB_ROOT_PASSWORD but only allows local connections"
echo ""

if [ "$CREATE_MYSQL_USER" = true ]; then
    echo "We also created"
    echo "A database called '${_userdb}' and"
    echo "a user called '${_user}' with password '${_userpass}'"
    echo "'${_user}' has full access on '${_userdb}'"
fi

echo "enjoy!"
echo "========================================================================"

mysqladmin -uroot -p$DB_ROOT_PASSWORD shutdown
