#!/bin/bash

# /usr/bin/mysqld_safe > /dev/null 2>&1 &

# RET=1
# while [[ RET -ne 0 ]]; do
#     echo "=> Waiting for confirmation of MySQL service startup"
#     sleep 5
#     mysql -e "status" > /dev/null 2>&1
#     RET=$?
# done

# #Import DB
# DB_MOODLE_DB_NAME=moodle
# DB_MOODLE_USER=moodleuser
# DB_MOODLE_PASSWORD=mypassword
# DB_MOODLE_DB_FILENAME=setup_database_eduarc-moodle-webapp.sql

# echo "========================================================================"

# # echo "Starting to import MOODLE DB..."
# # cat $DB_MOODLE_DB_FILENAME | mysql moodle

# echo "Import complete..."
# echo "enjoy!"
# echo "========================================================================"

echo "Now cloning the eduarcbox repo"

GITUSERNAME=`sed -n 1p gitlab-credentials.txt`
GITPASSWORD=`sed -n 2p gitlab-credentials.txt`

git clone https://${GITUSERNAME}:${GITPASSWORD}@git.informatik.uni-kiel.de/asal/eduarc-moodle-webapp.git /var/www/html/eduarc-moodle-webapp
# rm /var/www/html/eduarc-moodle-webapp/config.php
# mv /config.php /var/www/html/eduarc-moodle-webapp/config.php

echo "repo clone done and config.php pasted."

echo "========================================================================"
