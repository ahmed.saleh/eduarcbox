# Ansible in docker

**TODO** A wiki would be a better place for this kind of design documentations

## Short story

Pros:

- reuse MOVING configs
- more expressivness (dependency, order of installations etc.)

Cons:

- non-native way to install and configure things in docker
- re-provisioning every time containers are run

## More on this

Some articles that discribe why to use ansible together with Docker: 

1- https://www.ansible.com/blog/six-ways-ansible-makes-docker-compose-better 
2- https://nickjanetakis.com/blog/docker-and-ansible-solve-2-different-problems-and-they-can-be-used-together 

Beside [our ansible tutorial](http://mauricioklein.com/ansible/docker/2018/02/23/ansible-docker/), there are other huge amount of repositories for discussing about integration process. Some of them are explaining a bit why to do so, For example:
 
1- https://ruleoftech.com/2017/dockerizing-all-the-things-running-ansible-inside-docker-container 
2- https://medium.com/@tech_phil/running-ansible-inside-docker-550d3bb2bdff 

Also following this reddit article, helped me a bit to form an opinion:
 
https://www.reddit.com/r/docker/comments/9yt8mg/docker_vs_ansible/

Some developers are running ansible inside docker containers (like ours), while others are are running dockers with Ansible. In the later case, Ansible is the starting point of the project.

The first one is a more proper solution for our problem, as it was almost the exact same style that Sebastian used in MOVING (installing a vagrant container “or VM” with Vagrant, and then using “Ansible” on top of it to install some things). Of course, the main problems we had in MOVING was because of the vagrant Hyper-V limitaions, that’s why we decided to use dockers.

## Image for ansibled-service

- With the adapted image form the reference [tutorial](http://mauricioklein.com/ansible/docker/2018/02/23/ansible-docker/) (using keys instead of usr/pwd authentication), based on ubuntu 16.04, ansible installation succeds
	- hello world on localhost:5000
- With the adapted image (using keys instead of usr/pwd authentication), based on ubuntu 18.04, ansible installation succeds
	- hello world on localhost:5000

- With baseimage (which was the original idea as it is an adapted ubuntu 18.04 image to be more docker-friendly) the ansible provisioning fails due to a problem with ANSIX dependencies. Although this may not be the case with the real services to be configured with ansible (e.g. Elasticsearch) better to have a working solution that at this point, plus the advantage of baseimage in terms of optimizations are minor.
	- This may be related to this [issue](https://github.com/ANXS/build-essential/issues/11), although it was a problem in ubuntu 14.04

**Image space comparison**

- ubuntu 16.04 120 MB
- baseimage 183 MB
- **ubuntu 18.04 64.2 MB**

- ansibled-service with ubuntu 16.04 200 MB
	- still some space can be save removing the apt-cache and apt-lists
- ansibled-service with ubuntu baseimage 184 MB 
	- installation fails and likely stops so more space could be used by continuing the installation)
- ansibled-service with ubuntu 18.04 206 MB
	- still some space can be save removing the apt-cache and apt-lists
	- down to 179 MB cleaning apt-cache and apt-lists

Final choice is to use Ubuntu 18.04

