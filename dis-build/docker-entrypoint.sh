#!/bin/bash

echo "Waiting ES to get started"
#exec bin/elasticsearch

#exec utils/startes.sh &
sleep 60s
#exec utils/wait-for-it.sh --timeout=0 --strict localhost:9200 -- config/setup.sh
for i in {20..0}; do
	echo "Check if eduarc exists"
	httpCode=$(curl -I elasticsearch:9200/eduarc)
	if [[ ! ${httpCode+x} ]]; then
		break;
	fi
	sleep 2
done

echo "httpCode=$httpCode"


for i in {30..0}; do
if [[ $httpCode == *"404"* ]]; then
    echo "Index eduarc does not exist."
    curl -XPUT "elasticsearch:9200/eduarc?pretty" -H "Content-Type:application/json" --data-binary "@eduarc-index-settings.json"
    break;
fi
done

# map sample file to EduArc CDM
#echo "map sample file to EduArc CDM"
#exec java -jar ./files/dis/CDMMapper.jar -d econbiz -f ./files/sample/econbiz.json

echo "add sample data to index"
java -jar ./files/dis/CDMIndexer.jar -e eduarc -f ./files/sample -t 4
#add sample data to index

##echo "add hoou data to index"
##java -jar ./files/dis/CDMIndexer.jar -e eduarc -f ./files/sample/hoou-2020-07-30.json -t 4
##echo "add openclipart data to index"
##java -jar ./files/dis/CDMIndexer.jar -e eduarc -f ./files/sample/openclipart-2020-07-30.json -t 4
##echo "add weimar data to index"
##java -jar ./files/dis/CDMIndexer.jar -e eduarc -f ./files/sample/weimar-2020-08-03.json -t 4
##echo "add zoerr data to index"
##java -jar ./files/dis/CDMIndexer.jar -e eduarc -f ./files/sample/zoerr-2020-07-30.json -t 4
#exec java -jar ./files/dis/CDMIndexer.jar -e eduarc -f ./tmp/ready/econbiz.json -t 4 -ds econbiz
tail -f /dev/null
############################
#for i in {30..0}; do
#    if curl localhost:9200; then
#	curl -XPUT "localhost:9200/eduarc?pretty" -H "Content-Type:application/json" --data-binary "@eduarc-index-settings.json"
#        break;
#    fi
#    sleep 2
#done

#exec utils/wait-for-it.sh --timeout=0 --strict elasticsearch:9200 -- config/setup.sh

###########################
#curl elasticsearch:9200/eduarc
#if (( $? > 0 )); then
#    exec utils/wait-for-it.sh -t 0 elasticsearch:9200 -- config/setup.sh
#fi

#[ ! curl elasticsearch:9200/eduarc ] && exec utils/wait-for-it.sh -t 0 elasticsearch:9200 -- config/setup.sh

#exec  bin/elasticsearch & utils/wait-for-it.sh -t 0 localhost:9200 -- curl -XPUT "localhost:9200/eduarc?pretty" -H "Content-Type:application/json" --data-binary "@eduarc-index-settings.json" & tail -f /dev/null

#exec utils/wait-for-it.sh --timeout=0 --strict localhost:9200 -- curl -XPUT "localhost:9200/eduarc?pretty" -H "Content-Type:application/json" --data-binary "@eduarc-index-settings.json"

#tail -f /dev/null

#for i in {30..0}; do
#    if curl localhost:9200; then
#	curl -XPUT "localhost:9200/eduarc?pretty" -H "Content-Type:application/json" --data-binary "@eduarc-index-settings.json"
#        break;
#    fi
#    sleep 2
# done

#echo "keep ES up"
#tail -f /dev/null

# map sample file to EduArc CDM
#echo "map sample file to EduArc CDM"
#exec java -jar ./files/dis/CDMMapper.jar -d econbiz -f ./files/sample/econbiz.json

#add sample data to index
#exec java -jar ./files/dis/CDMIndexer.jar -e eduarc -f ./tmp/ready/econbiz.json -t 4 

