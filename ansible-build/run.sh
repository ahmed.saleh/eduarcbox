#!/bin/bash

# The commands to run in the ansible container

# Test if your container is accessible
# The return value of the coming command is $? . 0 indicates success, other indicates error
ansible all -m ping 
if [ $? -eq 0 ]; then
	echo 'Succeded' 
else
	echo '###### Permissions error detected -> the following commands will fix it ######'
	# To fix this permissions error, we will run the following commands
	chown root /root/.ssh
	chmod 400 /root/.ssh/id_rsa
fi

# Run the ping command again to debug if the permissions problem is resolved
ansible all -m ping 
if [ $? -eq 0 ]; then
	echo '###### Permissions error resolved ######' 
else
	echo '###### Permissions error still exist ######'
fi


# Download some ansible roles from Ansible Galaxy
# ANXS.git:           role to install the Git binary
# geerlingguy.git:    role to operate Git (i.e. download repository)
# geerlingguy.nodejs: role to install NodeJS
# ansible-galaxy install ANXS.git geerlingguy.git geerlingguy.nodejs

#Fire the playbook in verbose mode
#ansible-playbook -vvvv /root/playbooks/setup.yml
#Fire the playbook
ansible-playbook /root/playbooks/setup.yml

#echo '###### Test elasticsearch service ######'
#service elasticsearch status
